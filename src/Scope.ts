export enum Scope {
    GLOBAL = 'GLOBAL',
    LOCAL_STORAGE = 'LOCAL_STORAGE'
}