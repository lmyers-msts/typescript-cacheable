export class Dwarf extends Object {

    constructor(
        readonly firstName: string,
        readonly lastName: string) {
        super();
    }

}